<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('admin320/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ ('admin320/dist/css/adminlte.min.css') }}">
</head>

<body id="page-top">
    <div id="wrapper">
      <!-- Memanggil Sidebar pada view layout sidebar -->
      @include('layout.sidebar')
      <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
          <!-- Memanggil Sidebar pada view layout navbar -->
          @include('layout.navbar')
  
          <!-- Awal halaman -->
          <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <h1 class="h3 mb-0 text-gray-800">@yield('title')</h1>
            </div>
            <!-- Memanggil konten -->
            @yield('contents')
          </div>
        </div>
      </div>
    </div>